import matplotlib.pyplot as plt
from collections import Counter
import numpy as np
import pylab as pl
import sys, os
from terminaltables import AsciiTable
import argparse

parser = argparse.ArgumentParser(description='A tool for processing data from battery measurements.')

time_dict = {
    's' : 1,
    'm' : 60,
    'h' : 60*60,
}

fancy_names = {
    "pow" : ("Power", "W"),
    "vol" : ("Voltage", "V"),
    "cur" : ("Current", "A"),
    "none": ("should not happen", "xD")
}

def load_file(filename):
    file = open(filename)
    
    lines = file.read().splitlines()
    
    current_arr, time_arr, voltage_arr = [],[],[]
    
    for line in lines:
        line_split = line.split(',')
        t, v, c = int(line_split[0]), float(line_split[1]), float(line_split[2])
        current_arr.append(c)
        voltage_arr.append(v)
        time_arr.append(t)
        
    legend = os.path.basename(filename).split('.')[0]
    
    return np.array(current_arr, dtype='float'), np.array(time_arr, dtype='int'), np.array(voltage_arr, dtype='float'), legend

def smooth(array, scale = 'sec'):
    
    divisor = time_dict[scale]
    if divisor == 1: return array
    
    smoothed = []
    for i in range(0, len(array), divisor):
        top = i+divisor-1
        if top > len(array): top = len(array)
        mean = np.mean(array[i:top])
        smoothed.append(mean)
    
    return np.array(smoothed, dtype='float')

def table_line(current_arr, time_arr, voltage_arr, legend, timescale):
    line = []
    
    line.append(legend)
    
    line.append("%.3f" % np.min(voltage_arr))
    line.append("%.3f" % np.mean(voltage_arr))
    line.append("%.3f" % np.max(voltage_arr))
    
    line.append("%.3f" % np.min(current_arr))
    line.append("%.3f" % np.mean(current_arr))
    line.append("%.3f" % np.max(current_arr))
    
    duration = time_arr[-1]-time_arr[0]
    capacity = (duration/(60*60))*np.mean(current_arr)
    line.append("%.3f" % capacity)
    
    energy = np.sum(voltage_arr*current_arr)/3600
    line.append("%.3f" % energy)
    
    divisor = time_dict[timescale]
    line.append("%.2f" % (duration/divisor))
    
    return line

if __name__ == '__main__':
    parser.add_argument('--timescale', '-t', type=str, choices=['s', 'm', 'h'], help='Timescale to use in tables and graphs', default='m')
    parser.add_argument('input_files', help='Path to the input file', nargs='+')
    parser.add_argument('--output', '-o', help="Path to output the graph", type=str, default='bat.png')
    parser.add_argument('--graph', '-g', help="What to graph", type=str, default='vol', choices=('vol', 'cur', 'pow', 'none'), nargs='+')
    args = parser.parse_args()
    
    files = args.input_files
    timescale = args.timescale
    output_file = args.output
    to_graph_arg = args.graph

    plt.figure(figsize=(10, 8))
    plt.xlabel(f"Time [{timescale}]")
    
    to_graph = []
    to_graph.append(to_graph_arg)

    print(to_graph)

    label = ""
    for graph in to_graph:
        if graph == "none": continue
        label += f"{fancy_names[graph][0]} [{fancy_names[graph][1]}]  "
    
    plt.ylabel(label)
    plt.title("Discharge")
    
    table_lines = [["Run", "\nMin", "Voltage\nMean", "\nMax", "\nMin","Current\nMean","\nMax", "Capacity", "Energy", "Duration"]]
    
    for filename in files:
        current_arr, time_arr, voltage_arr, legend = load_file(filename)
        power_arr = current_arr * voltage_arr
        
        line = table_line(current_arr, time_arr, voltage_arr, legend, timescale)
        table_lines.append(line)
        for graph in to_graph:
            if graph == 'none' : continue
            if graph == 'vol'  : curve = voltage_arr
            if graph == 'cur'  : curve = current_arr
            if graph == 'pow'  : curve = power_arr
            
            curve = smooth(curve, timescale)
            plt.plot(curve, label=f"{legend}_{graph}")
        
        

    table_lines.append(["", "", "", "", "", "", "", "", ""])
    table_lines.append(["", "[V]", "[V]", "[V]", "[A]", "[A]", "[A]", "[Ah]", "[Wh]", f"[{timescale}]"])
    
    if not "none" in to_graph:
        bottom, top = plt.ylim()
        plt.ylim(bottom=0, top = top+0.1)
        plt.legend()
        plt.savefig(output_file)
    
    table = AsciiTable(table_lines)
    print(table.table)
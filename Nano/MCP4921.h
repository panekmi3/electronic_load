#ifndef MCP4921_header
#define MCP4921_header

#include <stdint.h>
#include "Arduino.h"

class MCP4921
{
private:
    uint8_t m_CS_PIN;
    uint8_t m_SCK_PIN;
    uint8_t m_SDI_PIN;

    const uint8_t m_header = 0b0011;

    void toggleClock();
    void sendData(uint16_t data, uint8_t n);
public:
    MCP4921(uint8_t CS, uint8_t SCK, uint8_t SDI);
    ~MCP4921();

    void analogWrite(uint16_t value);
};
#endif
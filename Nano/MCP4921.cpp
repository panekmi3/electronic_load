#include"MCP4921.h"

MCP4921::MCP4921(uint8_t CS_PIN, uint8_t SCK_PIN, uint8_t SDI_PIN)
{
    this->m_CS_PIN = CS_PIN;
    this->m_SCK_PIN = SCK_PIN;
    this->m_SDI_PIN = SDI_PIN;

    pinMode(CS_PIN, OUTPUT);
    pinMode(SCK_PIN, OUTPUT);
    pinMode(SDI_PIN, OUTPUT);

    analogWrite(0);
}

MCP4921::~MCP4921()
{
    this->analogWrite(0);
}

void MCP4921::toggleClock()
{
    digitalWrite(m_SCK_PIN, HIGH);
    digitalWrite(m_SCK_PIN, LOW);
    
    delayMicroseconds(10);
}

void MCP4921::sendData(uint16_t data, uint8_t n)
{
    for (int i = n-1; i >= 0; i--)
    {
        digitalWrite(m_SDI_PIN, (data & (1<<i)) >> i );
        toggleClock();
    }
}

void MCP4921::analogWrite(uint16_t value)
{
    digitalWrite(m_CS_PIN, LOW);
    sendData(m_header, 4);
    sendData(value, 12);
    digitalWrite(m_CS_PIN, HIGH);
}

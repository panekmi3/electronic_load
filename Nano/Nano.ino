#include <math.h>
#include <INA226.h>
#include "SPI.h"

#include "defines.h"
#include "MCP4921.h"

MCP4921 AnalogOutput(CS_PIN, SCK_PIN, MOSI_PIN);
INA226 ina;

float setCurrent = 0;
float setPower = 0;
float setResistance = 0;
float setVoltage = 0;

float voltageLimit = 0;
bool voltageLimitEnabled = false;

float current = 0;
float voltage = 0;
float power = 0;
int value = 0;

int fanSpeed = 0;

uint8_t mode = MODE_NONE;
uint8_t error = NO_ERROR;

bool debug = false;

typedef __attribute__((packed)) struct 
{
  uint64_t timeStamp;
  uint8_t state;
  float current;
  float voltage;
  uint8_t temp;
} entry;

struct __attribute__((packed)) command  
{
  uint8_t type;
  float value;
};

entry myEntry;

void updateCC()
{
  if(current > setCurrent && value != 0)
  {
    value--;
  }
  else if( current < setCurrent && value < 4095 )
  {
    value++;
  }
}

void updateCP()
{
  if( power >  setPower && value != 0 )
    value--;
  else if( power < setPower && value < 4095 )
    value++;
}

void updateCR()
{
  float resistance = voltage/current;

  if(resistance > setResistance && value < 4095 )
    value++;
  else if( resistance < setResistance && value != 0 )
    value--;
}

void updateCV()
{
  if( voltage > setVoltage && value < 4095 )
    value++;
  else if( voltage < setVoltage&& value != 0 ) 
    value--;
}

void checkVoltage()
{
  if(voltage < 0.5)
  {
    mode = MODE_NONE;
    error = ERROR_NO_VOLTAGE;
    return;
  }
}

void checkCurrent()
{
  current = ina.readShuntCurrent();
  if( value > 4000 && current < 0.001)
  {
    mode = MODE_NONE;
    error = ERROR_NO_CURRENT;
    return;
  }
}

void checkTemp()
{
  int temp1_raw  = analogRead(TEMP1_PIN);
  int temp2_raw  = analogRead(TEMP2_PIN);
  
  if( abs( temp1_raw - temp2_raw ) > 100 )
  {
    mode = MODE_NONE;
    error = ERROR_TEMP_MISMATCH;
    return;
  }
  if( temp1_raw > TEMP_THRESHOLD)
  {
    mode = MODE_NONE;
    error = ERROR_TEMP_OVER;
    return;
  }
}

void checkFan()
{
  static unsigned int sinceLastChange = 0;
  static bool lastState = false;
  
  bool state = digitalRead(TACHO_pin);

  if( lastState != state )
    sinceLastChange = 0;
  else
    sinceLastChange++;
  
  if( sinceLastChange > 1000)
  {
    sinceLastChange = 0;
    mode = MODE_NONE;
    error = ERROR_FAN;
  }

  lastState = state;

}

void checkMaxCurrent()
{
  if( current > INA_MAX_CURRENT && value )
  {
    value--;
  }
}

void update()
{ 
  current = ina.readShuntCurrent();
  voltage = ina.readBusVoltage();
  power = current * voltage;
  
  switch (mode)
  {
  case MODE_NONE:
    value = 0;
    break;
  case MODE_CC:
    updateCC();
    break;
  case MODE_CP:
    updateCP();
    break;
  case MODE_CR:
    updateCR();
    break;
  case MODE_CV:
    updateCV();
    break;
  default:
    break;
  }

  checkMaxCurrent();

  fanSpeed = readTemp();
  if (fanSpeed > 79) fanSpeed = 79;
  
  AnalogOutput.analogWrite(value);

  if(mode)
  {
    checkVoltage();
    checkCurrent();
    checkTemp();
    checkFan();
    underVoltageAlert();
    checkMaxCurrent();
  }
}

uint8_t readTemp()
{
  int raw = analogRead(TEMP2_PIN);
  uint8_t temp = raw * ( 100 * ADC_REF / ADC_RES );
  return temp;
}

void underVoltageAlert()
{
  if (voltage < voltageLimit)
  {
    mode = MODE_NONE;
    value = 0;
    error = ERROR_UNDERVOLTAGE;
  }
}

void setUnderVoltageLimit(float voltage)
{
  voltageLimit = voltage;
}

void resetLimit()
{
  voltageLimitEnabled = false;
}

uint32_t preValue(float current)
{
  current*=1000;
  return 800 * log10(current + 100) + 700;
}

void readSerial()
{
  static byte buffer[sizeof(command)];
  size_t r = 0;

  while ( Serial.available() > 0 && r < sizeof(command))
  { 
    buffer[r++] = Serial.read();
  }
  if( r > 0 )
  {
    command * cmd;
    cmd = (command * ) buffer;

    if(cmd->type < 9)
      mode = cmd->type;
    
    float targetCurrent;
    switch (cmd->type)
    {
    case MODE_NONE:
      value = 0;
      break;
    case MODE_CC:
      setCurrent = cmd->value;
      value = preValue(setCurrent);
      break;
    case MODE_CP:
      setPower = cmd->value;
      targetCurrent = setPower/voltage;
      value = preValue(targetCurrent);
      break;
    case MODE_CR:
      setResistance = cmd->value;
      targetCurrent = voltage/setResistance;
      value = preValue(targetCurrent);
      break;
    case MODE_CV:
      setVoltage = cmd->value;
      value = preValue(0);
      break;
    case SET_UNDERVOLTAGE_LIMIT:
      setUnderVoltageLimit(cmd->value);
      break;
    default:
      break;
    }
  }
}

void sendSerial(uint64_t timestamp)
{
  myEntry.state = error;
  myEntry.current = current;
  myEntry.voltage = voltage;
  myEntry.timeStamp = timestamp;
  myEntry.temp = readTemp();

  Serial.write((byte*) &myEntry, sizeof(entry));
  
  error = NO_ERROR;
}

void fanSetup()
{
  pinMode(PWM_pin, OUTPUT);
  pinMode(TACHO_pin, INPUT_PULLUP);
  TCCR2A = 0x23;
  TCCR2B = 0x0A;
  OCR2A = 79;
  OCR2B = 0;
}

void setup() 
{
  Serial.begin(115200);
  value = 0;
  ina.begin();
  ina.configure(INA226_AVERAGES_1, INA226_BUS_CONV_TIME_1100US, INA226_SHUNT_CONV_TIME_1100US, INA226_MODE_SHUNT_BUS_CONT);
  ina.calibrate(SHUNT_RESISTANCE, INA_MAX_CURRENT);

  fanSetup();

  analogReference(INTERNAL);
  
  delay(10);
}

void loop() 
{
  OCR2B = fanSpeed;

  readSerial();
  update();
  
  static uint64_t lastMillis = 0;
  uint64_t mil = millis();
  
  if( mil - lastMillis > 100 )
  {
    sendSerial(mil);
    lastMillis = mil;
  }
}

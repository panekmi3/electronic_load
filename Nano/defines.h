#define TEMP1_PIN A7
#define TEMP2_PIN A6
#define ADC_RES 1023.0
#define ADC_REF 1.1
#define TEMP_THRESHOLD 800  //cca 80 °C

#define SHUNT_RESISTANCE 0.0102
#define INA_MAX_CURRENT 6

#define PWM_pin 3
#define TACHO_pin 9

#define MOSI_PIN 12
#define SCK_PIN 13
#define CS_PIN 10

#define ALERT_PIN 2

#define MODE_NONE 0
#define MODE_CC 1
#define MODE_CP 2
#define MODE_CR 3
#define MODE_CV 4

#define SET_UNDERVOLTAGE_LIMIT 5

#define NO_ERROR 0
#define RUNNING 1
#define ERROR_UNDERVOLTAGE 2
#define ERROR_TEMP_OVER 3
#define ERROR_TEMP_MISMATCH 4
#define ERROR_FAN 5
#define ERROR_NO_VOLTAGE 6
#define ERROR_NO_CURRENT 7

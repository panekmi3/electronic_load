# Programable electronic load

## Capabilities

The load features continuous current, voltage, resistance and power modes. The maximum permissible current is 5 A, with 30 V maximum voltage for the maximum power of 150 W. The maximum power is also affected by the mounted cooler. The load allows for standard CPU coolers fot he LGA1155 and AM3 platforms. The user interface consists of a 2.4 inch LCD display and a rotary encoder with several buttons for control. The load can send the measured data over USB. WiFi or BlueTooth. It can also receive control commands through these means. The measured data can also be stored to an SD card.

![Load](./doc/Load.jpg)

## Hardware design

### Circuit design

The Devices uses two microcontrollers, one for user interface and one for controlling the analog circuit. On the Interface side is an ESP32 based microcontroller running FreeRTOS handling user input, rendering to the display and wireless communication. On the other side is an Arduino Pro Mini handling the current limiting.

![Digital scheme](./doc/Digital.PNG)

The Arduino controls an MCP4921 DAC wich drives the current regulating transistor through a series of op-amps. The Mini also uses the INA226 current monitor to read the current and voltage. These measurements are used to regulate the current passing through. 

![Analog scheme](./doc/Analog.PNG)

### PCB Design

The PCB wa designed using KiCad 7.0 and manufactured by JLC PCB.

![PCB Design](./doc/Design.PNG)

## Battery measurements

The data was measured using the "Save to SD" functionality. The measured data is saved as a text file with three columns, the time, voltage and current. These files can be processes using the python script into graphs and tables. The following image is an example of the load being used to test a standard Li-ion 18650 cell.


![Discharge graph](./doc/18650.png)
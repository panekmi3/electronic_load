#include "TFT_22_ILI9225.h"
#include "SD.h"
#include <math.h>
#include "Encoder.h"
#include <HardwareSerial.h>
#include <EEPROM.h>

#include "defines.h"

#define USE_WIFI 1

#if defined(USE_BLUE)
  #include "BluetoothSerial.h"
#endif //USE_BLUE

#if defined(USE_WIFI)
  #include "CWifi.h"
#endif //USE_WIFI

SPIClass hspi(HSPI);
SPIClass vspi(VSPI);

TFT_22_ILI9225 tft = TFT_22_ILI9225(TFT_RST, TFT_RS, TFT_CS, 0, 10);

Encoder myEnc(DT_PIN, CL_PIN);

uint8_t BTN_PINS          [4] = {36, 39, 34, 35};
uint8_t lastButtonStates  [4] = {1, 1, 1, 1};
uint8_t lastButtonsPressed[4] = {0, 0, 0, 0};

struct __attribute__((packed)) entry
{ 
  uint8_t temp;
  uint64_t timeStamp;
  uint8_t state;
  float current;
  float voltage;
};

struct __attribute__((packed)) command  
{
  uint8_t type;
  float value;
};

struct measurement
{
  uint8_t mode;
  float value;
  uint8_t stopCondition;
  long unsigned duration;
  float cuttofVoltage;
};

measurement m;
bool newMeasurement = false;

command limitCommand;
command mesCommand;
bool newCommands = false;

char * mode_srts[] = {"NONE", "CC", "CP", "CR", "CV"};
char * mode_unit_srts[] = {"NONE", " A", " W", " R", " V"};

float current = 0, voltage = 0, temp = 0, energy = 0, capacity = 0;
long int encoderValue = 0;
float selectedValue = 0;
bool active = false;
bool showCapacity = false;

uint8_t selectedStopCondition = 0;
char * stopConditions_strs[] = {"Manual", "Time", "Voltage"};
float selectedCuttofVoltage = 0;
int selectedDuration = 0;
int setVoltage = 0;

bool settingStopConditionValue = false;

bool selectedRecordToSD = false;
bool sdPresent = false;

hw_timer_t *My_timer = NULL;
volatile long long unsigned int seconds = 0;
int multiplier = 1;

uint8_t selectedMode = 1;
uint8_t error = NO_ERROR;
String SSID, PASS;
#if defined(USE_WIFI)
  String my_ip;
  bool wifi_connected = false;
  bool useWifi = true;
  
  CWifi wifi;
#endif //USE_WIFI


#if defined(USE_BLUE)
  BluetoothSerial SerialBT;
  bool blueActive = false;
#endif //USE_BLUE

void setPinModes()
{
  pinMode(GPIO_NUM_23, INPUT_PULLUP);
  for (size_t i = 0; i < 4; i++)
  {
    pinMode(BTN_PINS[i], INPUT);
  }
  pinMode(RESET_PIN, OUTPUT);
  pinMode(SW_PIN, INPUT);
  delay(5);
}

void resetLoad()
{
  digitalWrite(RESET_PIN, LOW);
  delay(10);
  digitalWrite(RESET_PIN, HIGH);
}

String highlight(int secs)
{
  if(multiplier == 1)     return "    _";
  if(multiplier == 10)    return "   _ ";
  if(multiplier == 100)   return " _   ";
  if(multiplier == 1000)  return "_    ";
}

String highlight(int n, int len, int decimal)
{
  int pos = len - log10(n);
  int dotPos = len - decimal;
  if(settingStopConditionValue) dotPos = len - 2;
  if(pos == dotPos) pos--;
  
  String ret = "";
  for (size_t i = 0; i <= len; i++)
    ret+=" ";
  
  
  ret[pos] = '_';
  return ret;  
}

String secsToMins(int secs)
{
  String seconds = String( secs % 60);
  if (secs%60<10)
    seconds = "0" + seconds;
  String minutes = String( secs / 60);
  if (secs/60<10)
    minutes = "0" + minutes;
  return minutes + ":" + seconds;
}

void drawStopConditions()
{
  tft.drawText(ILI9225_LCD_HEIGHT - 55, 90, "       ", COLOR_BLACK);
  tft.drawText(ILI9225_LCD_HEIGHT - 55, 90, stopConditions_strs[selectedStopCondition], COLOR_WHITE);

  tft.drawText(ILI9225_LCD_HEIGHT - 55, 100, "      ", COLOR_BLACK);
  tft.drawText(ILI9225_LCD_HEIGHT - 55, 101, "      ", COLOR_BLACK);
  if (selectedStopCondition == 1) 
  {
    if(settingStopConditionValue) tft.drawText(ILI9225_LCD_HEIGHT - 55, 101, highlight((int)selectedDuration), COLOR_WHITE);
    tft.drawText(ILI9225_LCD_HEIGHT - 55, 100, secsToMins(selectedDuration), COLOR_WHITE);
  }
  if (selectedStopCondition == 2) 
  {
    if(settingStopConditionValue) tft.drawText(ILI9225_LCD_HEIGHT - 55, 101, highlight(multiplier, String(selectedCuttofVoltage).length(), 2), COLOR_WHITE);
    tft.drawText(ILI9225_LCD_HEIGHT - 55, 100, String(selectedCuttofVoltage), COLOR_WHITE);
  }
}

void drawMeasurements()
{
  tft.setFont(Terminal12x16);

  float dispCurrent = current;
  
  if (current < 0) 
  dispCurrent = 0;

  String resistance;
  float resistance_f = voltage/dispCurrent;
  if (resistance_f > 1000 || resistance_f == INFINITY || resistance_f == NAN)
    resistance = ">1000";
  else
    resistance = String(resistance_f, 2);

  tft.drawText(10, 15, "                ", COLOR_BLACK);
  tft.drawText(10, 15, String(dispCurrent, 3) + " A", COLOR_RED);
  tft.drawText(10, 35, "                ", COLOR_BLACK);
  tft.drawText(10, 35, String(voltage, 2) + " V", COLOR_GREEN);
  tft.drawText(10, 55, "                ", COLOR_BLACK);
  tft.drawText(10, 55, String(dispCurrent * voltage, 2) + " W", COLOR_YELLOW);
  tft.drawText(10, 75, "                ", COLOR_BLACK);
  tft.drawText(10, 75, resistance + " R", COLOR_BLUE);
  tft.drawText(10, 95, "                ", COLOR_BLACK);
  tft.drawText(10, 95, showCapacity?(String(capacity) + " Ah"):(String(energy) + " Wh"), COLOR_BLUE);

  tft.setFont(Terminal6x8);

  tft.drawText(120, 0, "    ", COLOR_BLACK);
  tft.drawText(120, 0, String(temp, 1) + " C", COLOR_RED);

}

void updateLcd( void *pvParameters )
{
  char string[100];
  int buttonHeight = ILI9225_LCD_WIDTH / 4 -1;
  int buttonWidth = 60;

  for (size_t i = 0; i < 4; i++)
    tft.drawRectangle( ILI9225_LCD_HEIGHT - buttonWidth, buttonHeight*i, ILI9225_LCD_HEIGHT-1, buttonHeight*i +buttonHeight, COLOR_WHITE);

  tft.setFont(Terminal6x8);
  tft.drawText(ILI9225_LCD_HEIGHT - 55, 140, "Save SD", COLOR_WHITE);
  
  for(;;)
  {

    #if defined(USE_WIFI)
      tft.drawBitmap(0, 0, wifi_icon, 20, 15, wifi_connected? COLOR_GREEN:COLOR_RED);
      tft.drawText(20, 0, my_ip, wifi_connected? COLOR_GREEN:COLOR_RED);
    #endif //USE_WIFI
    #if defined(USE_BLUE)
      tft.drawBitmap(0, 0, blue_icon, 15, 20, COLOR_BLUE);
    #endif //USE_BLUE

    drawMeasurements();  

    tft.setFont(Terminal12x16);

    tft.drawText(ILI9225_LCD_HEIGHT - 40, 55, "   ", COLOR_BLACK);
    tft.drawText(ILI9225_LCD_HEIGHT - 40, 55, mode_srts[selectedMode], COLOR_WHITE);

    tft.setFont(Terminal12x16, true);
    tft.drawText(10, 115, "         ", COLOR_VIOLET);
    tft.drawText(10, 118, "         ", COLOR_VIOLET);
    if(! settingStopConditionValue)
      tft.drawText(10, 118, highlight(multiplier, String(selectedValue).length(), 3), COLOR_WHITE);
    tft.drawText(10, 115, String(selectedValue, 3) + mode_unit_srts[selectedMode], COLOR_WHITE);
   
    tft.setFont(Terminal6x8, true);
    tft.drawText(ILI9225_LCD_HEIGHT - 50, 20, "     ", COLOR_BLACK);
    tft.drawText(ILI9225_LCD_HEIGHT - 50, 20, active?"Stop":"Start", COLOR_WHITE);

    drawStopConditions();

    tft.drawText(ILI9225_LCD_HEIGHT - 40, 155, "     ", COLOR_BLACK);
    tft.drawText(ILI9225_LCD_HEIGHT - 40, 155, selectedRecordToSD ? "Yes": "No", COLOR_WHITE);

    tft.drawText(1, 150, errors_strs[error], COLOR_RED);

    vTaskDelay(100);
  }
}

void readEnc( void *pvParameters )
{
  bool lastSwitchState = true, switchState;
  for(;;)
  {
    switchState = digitalRead(SW_PIN);
    
    if( switchState != lastSwitchState && switchState == false )
    {
      multiplier*=10;
      if (multiplier >1000)
        multiplier = 1;
    }

    lastSwitchState = switchState;

    if(settingStopConditionValue)
    {
      if(selectedStopCondition == 1)
      {
        //setting time
        int timeMultiplier = multiplier;
        if(multiplier == 100) timeMultiplier = 60;
        if(multiplier == 1000) timeMultiplier = 600;

        selectedDuration += myEnc.readAndReset() * ((timeMultiplier/2) + timeMultiplier%2);
        if(selectedDuration < 0) selectedDuration = 0;
        else if( selectedDuration > TIME_MAX) selectedDuration = TIME_MAX;
      }
      else if(selectedStopCondition == 2)
      {
        setVoltage += myEnc.readAndReset() * multiplier;
        if(setVoltage < 0)  setVoltage = 0;
        if(setVoltage > VOLTAGE_MAX) setVoltage = VOLTAGE_MAX;
        selectedCuttofVoltage = setVoltage/200.0;
      }
    }
    else
    {
      encoderValue += myEnc.readAndReset() * multiplier;
      if (encoderValue < 0)
        encoderValue = 0;

      if     (selectedMode == MODE_CC && encoderValue > CURRENT_MAX) encoderValue = CURRENT_MAX;
      else if(selectedMode == MODE_CP && encoderValue > POWER_MAX) encoderValue = POWER_MAX;
      else if(selectedMode == MODE_CR && encoderValue > RESISTANCE_MAX) encoderValue = RESISTANCE_MAX;
      else if(selectedMode == MODE_CV && encoderValue > VOLTAGE_MAX) encoderValue = VOLTAGE_MAX;
    
      selectedValue = encoderValue/2000.0;
    }
    
    vTaskDelay(10);
  }
}

void readBtns ( void *pvParameters )
{
for(;;)
  {
    for (size_t i = 0; i < 4; i++)
    {
      uint8_t r = digitalRead(BTN_PINS[i]);

      if( ! r && lastButtonsPressed[i] < 250) lastButtonsPressed[i]++;
      if( r  && lastButtonStates[i] != r )
      {
        //button was just released, check for a long pulse.
        bool isLong = lastButtonsPressed[i] > 30;
        lastButtonsPressed[i] = 0;

        handleButtons(i, isLong);
      }
        
      lastButtonStates[i] = r;
    }
    vTaskDelay(10);
  } 
}

void handleButtons(uint8_t i, bool isLong)
{
  if (isLong)
    i+=4;

  switch (i)
  {
  //short presses
  case 0:
    if(active)
    {
      stop();
    }
    else
    {
      m.mode = selectedMode;
      m.value = selectedValue;
      m.stopCondition = selectedStopCondition;
      m.duration = selectedDuration;
      m.cuttofVoltage = selectedCuttofVoltage;
    }

    newMeasurement = true;
    break;
  case 1:
    selectedMode = ( selectedMode % 4 ) + 1;
    encoderValue = 0;
    break;
  case 2:
    if( ! active)
    {
      if(settingStopConditionValue) settingStopConditionValue = false; 
      else selectedStopCondition = ( ( selectedStopCondition + 1) % 3);
    }
    break;
  case 3:

    selectedRecordToSD = ( ! selectedRecordToSD) && sdPresent;

    break;
  //long presses
  case 4:

    energy = 0;
    capacity = 0;
    error = NO_ERROR;
    break;

  case 5:
    showCapacity = ! showCapacity;
    break;
  case 6:
  if( ! active)
  {
    if(selectedStopCondition)
      settingStopConditionValue = true;
  }
    break;
  case 7:

    break;
  default:
    break;
  }
}

int handleSD(File file, long unsigned startTime)
{
  if( ! file) return 0;
  else
  {
    file.println(String((int)seconds - startTime)+ "," + String(voltage) + "," + String(current, 3));
    return 1;
  }
}

void stop()
{
  m.mode = MODE_NONE;
  m.value = 0;
  m.stopCondition = 0;
  m.cuttofVoltage = 0;
  m.duration = 0;

  newMeasurement = true;
}

void meassureTask(void * pvParameters)
{
  long unsigned int meassurmentStarTime = 0;
  long unsigned int lastSeconds = 0;
  long unsigned secondsRemaining = 0;
  bool writeToSD = false;
  bool time = false;
  File outputFile;
  unsigned long int counter = 0;
  for(;;)
  {
    if(seconds > lastSeconds)
    {
      String toSend = String(counter++) + "," + String(voltage) + "," + String(current, 3);
      
      Serial.println(toSend);

      sendWireless(toSend + "\r\n");
      
      if(active)
      {
        energy+= (current * voltage)/3600.0;
        capacity+= (current)/3600.0;

        if(time) selectedDuration = secondsRemaining;

        if( ! (--secondsRemaining))
        {
          error = ERROR_TIME_LIMIT;
          stop();
        }
        if(writeToSD) 
        {
          int ret = handleSD(outputFile, meassurmentStarTime);
          if ( ! ret )
          {
            error = ERROR_SD;
            stop();
          }
        }
      }
    }

    if(newMeasurement)
    {
      newMeasurement = false;
      active = m.mode;
      if(active) error = NO_ERROR;
      time = (m.stopCondition == 1);
      if(time) secondsRemaining = m.duration;

      limitCommand.type = SET_UNDERVOLTAGE_LIMIT;
      limitCommand.value = ( m.stopCondition == 2) ? m.cuttofVoltage : 0;

      mesCommand.type = m.mode;
      mesCommand.value = m.value;

      newCommands = true;

      if(writeToSD = selectedRecordToSD && active)
      {
        outputFile = SD.open("/data.txt", FILE_WRITE);
      }

      if(outputFile && ! active)
      {
        outputFile.close();
      }

    }

    lastSeconds = seconds;
    vTaskDelay(100);
  }
}

void sendWireless(String toSend)
{
  #if defined(USE_WIFI)
    wifi.send(toSend);
  #endif // USE_WIFI
  #if defined(USE_BLUE)
    SerialBT.write( (uint8_t * ) toSend.c_str(), toSend.length());
  #endif // USE_BLUE  
}

void serial2Handle( void * pvParameters)
{
  vTaskDelay(200);

  byte rec[sizeof(entry)];
  entry * entr;
  int lastSentCommand = 0;
  for(;;)
  {
    if( Serial2.available() >= sizeof(entry) )
    {
      for( int i = 0; i < sizeof(entry); i++)
      {
        rec[i] = (byte) Serial2.read();  
      }

      if(entr->state > 10) continue;

      entr = (( entry * ) rec);
      current = entr->current;
      voltage = entr->voltage;
      temp = entr->temp;
      if(entr->state)
      {
        stop();
        error = entr->state;
      }
    }

    if(newCommands)
    {
      runCommand(limitCommand);
      vTaskDelay(10);
      runCommand(mesCommand);
      newCommands = false;
    }

    vTaskDelay(10);
  }
}

void serial1Handle( void * pvParameters)
{
  char buffer[100];
  for (;;)
  {
    if(Serial.available() > 0)
    {
      delay(1);
      size_t read = Serial.readBytesUntil('#', buffer, 100);
      buffer[read] = 0;
      String recieved = buffer;

      if(decode(recieved))
        newMeasurement = true;
    }

    vTaskDelay(100);
  }
}

int decode(String s)
{
  if(s.length() > 90)
    return 0;  

  char command_str[100];
  strncpy(command_str, s.c_str(), s.length());

  char * mode_str = strtok(command_str, ",");
  if( ! mode_str ) return 0;

  if ( ! (mode_str[0] == 'C'))
    return 0;
  

  int mode = MODE_NONE;

  if      (mode_str[1] == 'C') mode = MODE_CC;
  else if (mode_str[1] == 'P') mode = MODE_CP;
  else if (mode_str[1] == 'R') mode = MODE_CR;
  else if (mode_str[1] == 'V') mode = MODE_CV;
  else if (mode_str[1] == 'N') mode = MODE_NONE;
  else                         return 0;

  char * value_str = strtok(NULL, ",");
  if( ! value_str ) return 0; 

  float value = atof(value_str);
  if (value == 0 && ! mode == MODE_NONE) return 0;

  char * endCond_str = strtok(NULL, ",");
  if( ! endCond_str ) return 0;

  uint8_t endCondition;
  if     (endCond_str[0] == 'N' ) endCondition = 0;
  else if(endCond_str[0] == 'T' ) endCondition = 1; 
  else if(endCond_str[0] == 'V' ) endCondition = 2;
  else                            return 0;

  char * endValue_str = strtok(NULL, ",");

  if( ! endValue_str ) return 0;

  float cutoffVolttage;
  long unsigned duration;

  if( endCondition == 1 ) duration = atoi(endValue_str);
  if( endCondition == 2 ) cutoffVolttage = atof(endValue_str);

  m.mode = mode;
  m.value = value;

  m.stopCondition = endCondition;

  if( endCondition == 1 ) m.duration = duration;
  if( endCondition == 2 ) m.cuttofVoltage = cutoffVolttage;

  return 1;
}

void runCommand(const command & cmd )
{
  Serial2.write( (byte * ) &cmd, sizeof(command));
}

void CreateTasks()
{
  xTaskCreatePinnedToCore(
    readEnc,
    "ENC",
    1024,
    NULL,
    1,
    NULL,
    1
  );

  xTaskCreatePinnedToCore(
    readBtns,
    "BTNS",
    1024,
    NULL,
    1,
    NULL,
    1
  );

  xTaskCreatePinnedToCore(
    serial1Handle,
    "SER1",
    1024,
    NULL,
    1,
    NULL,
    0
  );

  xTaskCreatePinnedToCore(
    serial2Handle,
    "SER2",
    1024,
    NULL,
    1,
    NULL,
    0
  );

  xTaskCreatePinnedToCore(
    meassureTask,
    "MES",
    8196,
    NULL,
    1,
    NULL,
    0
  );

  xTaskCreatePinnedToCore(
    updateLcd,
    "LCD",
    32768,
    NULL,
    2,
    NULL,
    1
  );

  xTaskCreatePinnedToCore(
    wirelesssTask,
    "Wrles",
    8196,
    NULL,
    2,
    NULL,
    1
  );
}

void IRAM_ATTR onTimer()
{
  seconds++;  
}

int readWifiInfo(String & SSID, String & PASS)
{
  File file = SD.open("/wifi.txt", FILE_READ);
  if ( ! file)
    return 0;
  SSID = file.readStringUntil('\n');
  int index = 0;
  if(index = SSID.lastIndexOf('\r')) SSID[index] = 0; 
  PASS = file.readStringUntil('\n');
  if(PASS[PASS.length()-1] == 13) PASS[PASS.length()] = 0;

  file.close();

  return 1;
}

void wirelesssTask( void * vParameters)
{
  String recieved;
  char buffer[100];
  buffer[99] = 0;
  bool rec = false;

  #if defined(USE_WIFI)
  
  wifi_connected = wifi.wifiConnect(SSID, PASS);
  my_ip = wifi.getIP();
  #endif // USE_WIFI

  #if defined(USE_BLUE)
  
    SerialBT.begin("Electronic load");
  
  #endif //USE_BLUE

  for(;;)
  {
    #if defined(USE_WIFI)
    
    if( wifi_connected )
    {
      recieved = wifi.wifiTask();
      rec = recieved.length();
    }
    
    #endif // USE_WIFI
    
    #if defined(USE_BLUE)
    
      if(SerialBT.available())
      {
        recieved = SerialBT.readStringUntil('#');
        rec = true;
      }
    
    #endif //USE_BLUE
    

    if(rec)
    {
      if(decode(recieved))
        newMeasurement = true;
    }
    rec = false;
  
    vTaskDelay(100);
  }
}

void setup() 
{
  Serial.begin(115200);
  Serial2.begin(115200, SERIAL_8N1, RXD2, TXD2);
  EEPROM.begin(512);
  setPinModes();

  hspi.begin(SD_SCK, SD_MISO, SD_MOSI, SD_CS);
  tft.begin(hspi);
  tft.setFont(Terminal12x16);
  tft.setOrientation(3);

  sdPresent = SD.begin(SD_CS,hspi,80000000);
  
  if(sdPresent && ! digitalRead(SW_PIN) && readWifiInfo(SSID, PASS))
  {
    EEPROM.writeByte(SSID_LEN_ADDRESS, SSID.length());
    EEPROM.writeBytes(SSID_ADDRESS, SSID.c_str(), SSID.length());

    EEPROM.writeByte(PASS_LEN_ADDRESS, PASS.length());
    EEPROM.writeBytes(PASS_ADDRESS, PASS.c_str(), PASS.length());
    EEPROM.commit();
  }
  else
  {
    char buffer[100];
    int len;
    
    len = EEPROM.readByte(SSID_LEN_ADDRESS);
    EEPROM.readBytes(SSID_ADDRESS, buffer, len );
    buffer[len] = 0;
    SSID = buffer;

    len = EEPROM.readByte(PASS_LEN_ADDRESS);
    EEPROM.readBytes(PASS_ADDRESS, buffer, len );
    buffer[len] = 0;
    PASS = buffer;
  } 
  
  My_timer = timerBegin(0, 80, true);
  timerAttachInterrupt(My_timer, &onTimer, true);
  timerAlarmWrite(My_timer, 1000000, true);
  timerAlarmEnable(My_timer);
  
  resetLoad();

  CreateTasks();
}

void loop()
{
  //empty
}

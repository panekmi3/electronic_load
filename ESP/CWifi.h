#ifndef CWFI_HEADER
#define CWFI_HEADER

#include <WiFiMulti.h>

class CWifi
{
private:
    WiFiMulti wifiMulti;
    WiFiServer server;
    WiFiClient serverClient;

    bool wifi_connected;
    String ssid, pass;
public:
    CWifi();
    ~CWifi();

    bool wifiConnect(String SSID, String PASS);
    void send(String s);
    String getIP();
    String wifiTask();
};

#endif
#include "CWifi.h"

CWifi::CWifi()
{
    this->server = WiFiServer(23);
}

CWifi::~CWifi()
{
}

bool CWifi::wifiConnect(String SSID, String PASS)
{
  wifiMulti.addAP(SSID.c_str(), PASS.c_str());
  
  if (wifiMulti.run() != WL_CONNECTED) 
    return false;
  
  server.begin();
  server.setNoDelay(true);  

  wifi_connected = true;
  return true;
}

String CWifi::getIP()
{
  return WiFi.localIP().toString();
}

void CWifi::send(String s)
{
  if ( wifi_connected && serverClient && serverClient.connected())
  {
    serverClient.write(s.c_str());
  }
}

String CWifi::wifiTask()
{
    String ret;
    if(server.hasClient())
    {
        if( ! serverClient || ! serverClient.connected())
        {
          if(serverClient) serverClient.stop();
          serverClient = server.available();
        }
    }
    if (serverClient && serverClient.connected())
    {
        if(serverClient.available())
        {
          if(serverClient.available())
            ret = serverClient.readStringUntil('#');
        }
        
    }
    else 
    {
        if (serverClient) 
        {
          serverClient.stop();
        }
    }

    return ret;
}
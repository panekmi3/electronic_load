#define TFT_RST 26  // IO 26
#define TFT_RS  25  // IO 25
#define TFT_CLK 14  // HSPI-SCK
#define TFT_SDI 13  // HSPI-MOSI
#define TFT_CS  33  // HSPI-SS0

#define SD_SCK  14
#define SD_MISO  12
#define SD_MOSI  13
#define SD_CS  27

//encoder pins
#define SW_PIN 19
#define DT_PIN 18
#define CL_PIN 5

#define RESET_PIN 32

#define RXD2 16
#define TXD2 17

#define MODE_NONE 0
#define MODE_CC 1
#define MODE_CP 2
#define MODE_CR 3
#define MODE_CV 4

#define SET_UNDERVOLTAGE_LIMIT 5

#define NO_ERROR 0
#define RUNNING 1
#define ERROR_UNDERVOLTAGE 2
#define ERROR_TEMP_OVER 3
#define ERROR_TEMP_MISMATCH 4
#define ERROR_FAN 5
#define ERROR_NO_VOLTAGE 6
#define ERROR_NO_CURRENT 7
#define ERROR_TIME_LIMIT 8
#define ERROR_SD 9

char * errors_strs[] = {
  "                     ",
  "RUNNING",
  "Voltage limit reached",
  "Temp limit reached",
  "Teampreature error",
  "Fan error",
  "Error, no voltage",
  "Error, no current",
  "Time limit reached",
  "SD error"
};

#define TIME_MAX 43200 //12 H
#define VOLTAGE_MAX 30000 //30 V
#define POWER_MAX 150000  //150 W
#define CURRENT_MAX 5000 //5 A
#define RESISTANCE_MAX 500000 //500 R


#define SSID_LEN_ADDRESS 0
#define SSID_ADDRESS 1
#define PASS_LEN_ADDRESS 100
#define PASS_ADDRESS 101

const unsigned char wifi_icon [] PROGMEM = {
0x01, 0x68, 0x00, 
0x07, 0xfe, 0x00, 
0x1f, 0xff, 0x80, 
0x7f, 0x0f, 0xe0, 
0x78, 0x01, 0xe0, 
0x70, 0x00, 0xe0, 
0x00, 0xf0, 0x00, 
0x03, 0xfc, 0x00, 
0x07, 0xfe, 0x00, 
0x07, 0x0e, 0x00, 
0x02, 0x00, 0x00, 
0x00, 0x00, 0x00, 
0x00, 0x60, 0x00, 
0x00, 0x70, 0x00, 
0x00, 0x60, 0x00
};

const unsigned char blue_icon [] PROGMEM = {
	0xfc, 0xfe, 0xfc, 0x3e, 0xfc, 0x1e, 0xfc, 0x86, 0xbc, 0xc2, 0x1c, 0xf0, 0x84, 0xe2, 0xc0, 0x86, 
	0xf0, 0x0e, 0xf8, 0x3e, 0xf8, 0x3e, 0xf0, 0x0e, 0xc0, 0x86, 0x84, 0xe2, 0x1c, 0xf0, 0xbc, 0xc2, 
	0xfc, 0x86, 0xfc, 0x1e, 0xfc, 0x3e, 0xfc, 0xfe
};
